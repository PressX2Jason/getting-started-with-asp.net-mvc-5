﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCMovie.Controllers
{
    public class HelloWorldController : Controller
    {
        //
        // GET: /HelloWorld/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /HelloWorld/Welcome?name=Scott&numtimes=4

        public ActionResult Welcome(string name, int numTimes = 1)
        {
            ViewBag.message = "Hello " + name;
            ViewBag.numTimes = numTimes;

            return View();
        }

    }
}
